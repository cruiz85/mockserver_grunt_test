# grunt_test

using grunt to scalfolding a node structure

## Getting Started
Install the module with: `npm install grunt_test`

```javascript
var grunt_test = require('grunt_test');
grunt_test.awesome(); // "awesome"
```

## Documentation
_(Coming soon)_

## Examples
_(Coming soon)_

## Contributing
In lieu of a formal styleguide, take care to maintain the existing coding style. Add unit tests for any new or changed functionality. Lint and test your code using [Grunt](http://gruntjs.com/).

## Release History
_(Nothing yet)_

## License
Copyright (c) 2015 Cesar Ruiz  
Licensed under the MIT license.
