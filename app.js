
var glosaryMock = require('./mocks/glossary.json');

var mockserver = require('mockserver-grunt');
var mockServerClient = require('mockserver-client').mockServerClient;

var HTTP_PORT = 1080;
var HTTP_PROXY_PORT = 1090;

mockserver.start_mockserver({
    serverPort: HTTP_PORT,
    proxyPort   : HTTP_PROXY_PORT,
    verbose : true
}).then(function(){

    mockServerClient("localhost", HTTP_PORT).mockSimpleResponse('/glossary', glosaryMock , 203);


    mockServerClient("localhost", HTTP_PORT).mockSimpleResponse('/somePath', { name: 'value' }, 203);


    mockServerClient("localhost", HTTP_PORT).mockAnyResponse({
        "httpRequest": {
            "path": "/rest/path",
            "headers": [
                {
                    "name": "Host",
                    "values": "localhost:1080"
                }
            ]
        },
        "httpForward": { // local machine Tomcat instance
            "host": "www.google.com",
            "port": 80,
            "scheme": "HTTP"
        },
        "times": {
            "unlimited": true
        }
    });

      // forward backend REST API request to local machine
    //mockServerClient("localhost", HTTP_PORT).mockAnyResponse({
    //    "httpRequest": {
    //        "path": "/rest.*",
    //        "headers": [
    //            {
    //                "name": "Host",
    //                "values": "qa\\.environment\\.com.*"
    //            }
    //        ]
    //    },
    //    "httpForward": { // local machine Tomcat instance
    //        "host": "127.0.0.1",
    //        "port": 8080,
    //        "scheme": "HTTP"
    //    },
    //    "times": {
    //        "unlimited": true
    //    }
    //});
    // forward all other request to QA environment
    //mockServerClient("localhost", HTTP_PORT).mockAnyResponse({
    //    "httpRequest": {
    //        "path": "/.*",
    //        "headers": [
    //            {
    //                "name": "Host",
    //                "values": "qa\\.environment\\.com.*"
    //            }
    //        ]
    //    },
    //    "httpForward": { // QA load balancer
    //        "host": "192.168.50.10",
    //        "port": 443,
    //        "scheme": "HTTPS"
    //    },
    //    "times": {
    //        "unlimited": true
    //    }
    //});

    //mockServerClient("localhost", HTTP_PORT).mockAnyResponse(
    //    {
    //        'httpRequest': {
    //            'method': 'POST',
    //            'path': '/somePathComplex',
    //            'queryStringParameters': [
    //                {
    //                    'name': 'test',
    //                    'values': [ 'true' ]
    //                }
    //            ],
    //            'body': {
    //                'type': 'STRING',
    //                'value': 'someBody'
    //            }
    //        },
    //        'httpResponse': {
    //            'statusCode': 200,
    //            'body': JSON.stringify({ name: 'value' }),
    //            'delay': {
    //                'timeUnit': 'MILLISECONDS',
    //                'value': 250
    //            }
    //        },
    //        'times': {
    //            'remainingTimes': 1,
    //            'unlimited': false
    //        }
    //    }
    //);
    //
    //mockServerClient("localhost", 1080).mockAnyResponse({
    //    "httpRequest": {
    //        "method": "GET",
    //        "path": "/login",
    //        "body": "{username: 'foo', password: 'bar'}"
    //    },
    //    "httpResponse": {
    //        "statusCode": 302,
    //        "headers": [
    //            {
    //                "name": "Location",
    //                "values": "https://www.mock-server.com"
    //            }
    //        ],
    //        "cookies": [
    //            {
    //                "name": "sessionId",
    //                "value": "2By8LOhBmaW5nZXJwcmludCIlMDAzMW"
    //            }
    //        ]
    //    }
    //});
});

console.log("started on port: " + HTTP_PORT);

// stop MockServer if Node exist abnormally
process.on('uncaughtException', function (err) {
    console.log('uncaught exception - stopping node server: ' + JSON.stringify(err));
    mockserver.stop_mockserver();
    throw err;
});

// stop MockServer if Node exits normally
process.on('exit', function () {
    console.log('exit - stopping node server');
    mockserver.stop_mockserver();
});

// stop MockServer when Ctrl-C is used
process.on('SIGINT', function () {
    console.log('SIGINT - stopping node server');
    mockserver.stop_mockserver();
    process.exit(0);
});

// stop MockServer when a kill shell command is used
process.on('SIGTERM', function () {
    console.log('SIGTERM - stopping node server');
    mockserver.stop_mockserver();
    process.exit(0);
});