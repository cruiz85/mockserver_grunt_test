//'use strict';

module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    start_mockserver: {
      start: {
        options: {
          serverPort: 1080,
          proxyPort: 1090
        }
      }
    },
    stop_mockserver: {
      stop: {

      }
    },
    nodeunit: {
      files: ['test/**/*_test.js'],
    },
    jshint: {
      options: {
        jshintrc: '.jshintrc'
      },
      gruntfile: {
        src: 'Gruntfile.js'
      },
      lib: {
        src: ['lib/**/*.js']
      },
      test: {
        src: ['test/**/*.js']
      },
    },
    watch: {
      gruntfile: {
        files: '<%= jshint.gruntfile.src %>',
        tasks: ['jshint:gruntfile']
      },
      lib: {
        files: '<%= jshint.lib.src %>',
        tasks: ['jshint:lib', 'nodeunit']
      },
      test: {
        files: '<%= jshint.test.src %>',
        tasks: ['jshint:test', 'nodeunit']
      },
    },
  });

  // These plugins provide necessary tasks.
  grunt.loadNpmTasks('grunt-contrib-nodeunit');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');

  //mock-server start task
  grunt.loadNpmTasks('mockserver-grunt');
  // Default task.
  grunt.registerTask('default', ['jshint', 'nodeunit']);

};
